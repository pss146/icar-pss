//
//  DataAdapter.m
//  icar
//
//  Created by Stanislav Perepelitsyn on 3/17/14.
//  Copyright (c) 2014 Stanislav Perepelitsyn. All rights reserved.
//

#import "DataAdapter.h"
#import "DataAdapter+DefaultData.h"
#import "Model/DataModel.h"

static const NSString* MODEL_NAME = @"DataModel";
static const NSString* DATABASE_NAME = @"icar.sqlite";

@implementation DataAdapter

#pragma mark - Singleton Methods

+ (DataAdapter*)sharedInstance {
    // Use GDC approach to make singletone
    static DataAdapter *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    if (self = [super init]) {
        // Init instance variables
        [MagicalRecord setupCoreDataStackWithStoreNamed:MODEL_NAME];
        [self createDefaultDataInDatabase];
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

#pragma mark - Class methods

-(void) saveContext {
    NSManagedObjectContext* context = [NSManagedObjectContext MR_contextForCurrentThread];
    [context saveToPersistentStoreAndWait];
    return;

    //NSManagedObjectContext * context = [NSManagedObjectContext defaultContext];
    [context saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (success) {
            NSLog(@"Successful saved context!");
        } else if (error) {
            NSLog(@"Error saving context: %@", error.description);
        }
    }];
}

- (CostCategory *)serviceCategory {
    CostCategory *result = [CostCategory findFirstByAttribute:CostCategoryAttributes.name withValue:SERVICE_CATEGORY_NAME];
    if (result == nil) {
        NSLog(@"No 'Service' category found");
    }
    return result;
}

- (CostCategory *)otherCategory {
    CostCategory *result = [CostCategory findFirstByAttribute:CostCategoryAttributes.name withValue:OTHER_CATEGORY_NAME];
    if (result == nil) {
        NSLog(@"No 'Other' category found");
    }
    return result;
}

@end
