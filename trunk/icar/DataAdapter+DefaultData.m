//
// Created by Stanislav Perepelitsyn on 3/17/14.
// Copyright (c) 2014 Stanislav Perepelitsyn. All rights reserved.
//

#import "DataAdapter+DefaultData.h"
#import "Model/DataModel.h"

static const NSString* DEFAULT_PARTSITEMS_JSON = @"DefaultPartItems";
static const NSString* DEFAULT_OTHERITEMS_JSON = @"DefaultOtherItems";


@implementation DataAdapter (DefaultData)

-(void)createDefaultDataInDatabase {
    // Check if default Categories exist

    //DEBUG: Delete all data
    [CostCategory deleteAllMatchingPredicate:nil];
    [Item deleteAllMatchingPredicate:nil];
    //

    NSArray* categories = [CostCategory findAll];
    if ([categories count] == 0) {
        NSLog(@"No Categories. Create default one --------------------");
        [self createDefaultCostCategories];
    } else {
        [categories enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSLog(@"Category: %@", [(CostCategory*)obj name]);
        }];
        NSLog(@"Delete all Categories --------------------");
        [CostCategory deleteAllMatchingPredicate:nil];
        [self saveContext];
    }
}

-(void) createDefaultCostCategories {
    //Service
    //TODO:
    CostCategory* serviceCategory = [CostCategory createEntity];
    serviceCategory.name = SERVICE_CATEGORY_NAME;
    [self createItemsInCategory:serviceCategory fromJSON:DEFAULT_PARTSITEMS_JSON];

    //Others
    //TODO:
    CostCategory* otherCategory = [CostCategory createEntity];
    otherCategory.name = OTHER_CATEGORY_NAME;
    [self createItemsInCategory:otherCategory fromJSON:DEFAULT_OTHERITEMS_JSON];

    [self saveContext];
}

-(void) createItemsInCategory:(CostCategory *)category fromJSON:(NSString*)jsonPath {
    if ([jsonPath length] == 0);

    // Create grouped items catalog
    // Catalog: Groups/Items ->> Groups/Items

    // Load JSON file contents
    NSError* err = nil;
    NSString* dataPath = [[NSBundle mainBundle] pathForResource:jsonPath ofType:@"json"];
    NSString *jsonString = [[NSString alloc] initWithContentsOfFile:dataPath encoding:NSUTF8StringEncoding error:&err];

    NSArray* items = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]
                                                     options:NSJSONReadingMutableContainers
                                                       error:&err];
    NSLog(@"JSON file: %@", dataPath);
    NSLog(@"JSON Items:");
    NSDictionary *mapObjectsToId = [[NSMutableDictionary alloc] initWithCapacity:items.count];
    [items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSLog(@"{id: %@, name: %@, pId:%@}", [obj objectForKey:@"id"], [obj objectForKey:@"name"], [obj objectForKey:@"pId"]);

        // Create and map new partItem to its id
        [self createParentForObject:obj inCategory:category usingMap:mapObjectsToId fromJSONArray:items];
    }];
}

- (PartItem *)createParentForObject:(id)obj inCategory:(CostCategory *)category usingMap:(NSDictionary *)map fromJSONArray:(NSArray *)array {
    if (obj == nil) {
        NSLog(@"!!!!!!! obj == nil !!!!!!!!");
        return nil;
    }
    NSNumber * id = [obj objectForKey:@"id"];
    NSNumber * pId = [obj objectForKey:@"pId"];

    // Check if value already exist -> might be created on prev step of recursion
    PartItem* partItem = [map valueForKey:[id stringValue]];
    if (partItem == nil) {
        //Create new item
        partItem = [self createPartItemWithObject:obj];
        partItem.category = category;
        [map setValue:partItem forKey:[id stringValue]];
        NSLog(@"Created item: %@, id: %@, pId: %@", partItem.name, id, pId);

        //Check if has parent
        if (pId != nil && pId.intValue !=0 ) {
            // Check if parent exist
            NSLog(@"Try to find parent with pId = %@", pId);
            PartItem* parent = [map valueForKey:[pId stringValue]];
            if (parent == nil) {
                NSLog(@"Parent not exist. Create new parent...");
                NSDictionary * obj2 = [self findObjectInArray:array matchingParentId:pId];
                parent = [self createParentForObject:obj2 inCategory:category usingMap:map fromJSONArray:array];
            }
            // Set parent item
            partItem.parent = parent;
        }
    }
    return partItem;
}

-(id) findObjectInArray:(NSArray *)array matchingParentId:(NSNumber *)pId {
    NSLog(@"Find array with pId: %@", pId);
    id result = nil;
    for (id obj in array) {
        NSNumber *id = [obj valueForKey:@"id"];
        NSNumber *pId2 = [obj valueForKey:@"pId"];
        NSString *name = [obj valueForKey:@"name"];
        if ([id intValue] == pId.intValue) {
            result = obj;
            NSLog(@"Found element, id:%@, name:%@, pId:%@", id, name, pId2);
            break;
        }
    };

    return result;
}

-(PartItem *) createPartItemWithObject:(id)obj {
    PartItem* item = [PartItem createEntity];
    [self initPartItem:item withObject:obj];
    return item;
}

-(void)initPartItem:(PartItem *)item withObject:(id)obj {
    item.name = [obj objectForKey:ItemAttributes.name];
}

@end