//
//  Utils.m
//  icar
//
//  Created by Stanislav Perepelitsyn on 3/17/14.
//  Copyright (c) 2014 Stanislav Perepelitsyn. All rights reserved.
//

#import "Utils.h"

@implementation Utils

// Returns the URL to the application's Documents directory.
+ (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}
@end
