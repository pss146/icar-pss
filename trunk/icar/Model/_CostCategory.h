// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CostCategory.h instead.

#import <CoreData/CoreData.h>


extern const struct CostCategoryAttributes {
	__unsafe_unretained NSString *name;
} CostCategoryAttributes;

extern const struct CostCategoryRelationships {
	__unsafe_unretained NSString *items;
} CostCategoryRelationships;

extern const struct CostCategoryFetchedProperties {
} CostCategoryFetchedProperties;

@class Item;



@interface CostCategoryID : NSManagedObjectID {}
@end

@interface _CostCategory : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (CostCategoryID*)objectID;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *items;

- (NSMutableSet*)itemsSet;





@end

@interface _CostCategory (CoreDataGeneratedAccessors)

- (void)addItems:(NSSet*)value_;
- (void)removeItems:(NSSet*)value_;
- (void)addItemsObject:(Item*)value_;
- (void)removeItemsObject:(Item*)value_;

@end

@interface _CostCategory (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;





- (NSMutableSet*)primitiveItems;
- (void)setPrimitiveItems:(NSMutableSet*)value;


@end
