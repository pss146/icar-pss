// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Other.m instead.

#import "_Other.h"

const struct OtherAttributes OtherAttributes = {
};

const struct OtherRelationships OtherRelationships = {
	.item = @"item",
};

const struct OtherFetchedProperties OtherFetchedProperties = {
};

@implementation OtherID
@end

@implementation _Other

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Other" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Other";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Other" inManagedObjectContext:moc_];
}

- (OtherID*)objectID {
	return (OtherID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic item;

	






@end
