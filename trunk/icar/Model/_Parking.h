// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Parking.h instead.

#import <CoreData/CoreData.h>
#import "CostItem.h"

extern const struct ParkingAttributes {
	__unsafe_unretained NSString *period;
} ParkingAttributes;

extern const struct ParkingRelationships {
} ParkingRelationships;

extern const struct ParkingFetchedProperties {
} ParkingFetchedProperties;




@interface ParkingID : NSManagedObjectID {}
@end

@interface _Parking : CostItem {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ParkingID*)objectID;





@property (nonatomic, strong) NSDate* period;



//- (BOOL)validatePeriod:(id*)value_ error:(NSError**)error_;






@end

@interface _Parking (CoreDataGeneratedAccessors)

@end

@interface _Parking (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitivePeriod;
- (void)setPrimitivePeriod:(NSDate*)value;




@end
