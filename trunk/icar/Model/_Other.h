// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Other.h instead.

#import <CoreData/CoreData.h>
#import "CostItem.h"

extern const struct OtherAttributes {
} OtherAttributes;

extern const struct OtherRelationships {
	__unsafe_unretained NSString *item;
} OtherRelationships;

extern const struct OtherFetchedProperties {
} OtherFetchedProperties;

@class OtherItem;


@interface OtherID : NSManagedObjectID {}
@end

@interface _Other : CostItem {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (OtherID*)objectID;





@property (nonatomic, strong) OtherItem *item;

//- (BOOL)validateItem:(id*)value_ error:(NSError**)error_;





@end

@interface _Other (CoreDataGeneratedAccessors)

@end

@interface _Other (CoreDataGeneratedPrimitiveAccessors)



- (OtherItem*)primitiveItem;
- (void)setPrimitiveItem:(OtherItem*)value;


@end
