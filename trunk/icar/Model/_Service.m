// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Service.m instead.

#import "_Service.h"

const struct ServiceAttributes ServiceAttributes = {
};

const struct ServiceRelationships ServiceRelationships = {
	.parts = @"parts",
};

const struct ServiceFetchedProperties ServiceFetchedProperties = {
};

@implementation ServiceID
@end

@implementation _Service

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Service" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Service";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Service" inManagedObjectContext:moc_];
}

- (ServiceID*)objectID {
	return (ServiceID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic parts;

	
- (NSMutableSet*)partsSet {
	[self willAccessValueForKey:@"parts"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"parts"];
  
	[self didAccessValueForKey:@"parts"];
	return result;
}
	






@end
