// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CostCategory.m instead.

#import "_CostCategory.h"

const struct CostCategoryAttributes CostCategoryAttributes = {
	.name = @"name",
};

const struct CostCategoryRelationships CostCategoryRelationships = {
	.items = @"items",
};

const struct CostCategoryFetchedProperties CostCategoryFetchedProperties = {
};

@implementation CostCategoryID
@end

@implementation _CostCategory

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"CostCategory" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"CostCategory";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"CostCategory" inManagedObjectContext:moc_];
}

- (CostCategoryID*)objectID {
	return (CostCategoryID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic name;






@dynamic items;

	
- (NSMutableSet*)itemsSet {
	[self willAccessValueForKey:@"items"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"items"];
  
	[self didAccessValueForKey:@"items"];
	return result;
}
	






@end
