// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Refuel.h instead.

#import <CoreData/CoreData.h>
#import "CostItem.h"

extern const struct RefuelAttributes {
	__unsafe_unretained NSString *brand;
	__unsafe_unretained NSString *isFull;
	__unsafe_unretained NSString *liters;
} RefuelAttributes;

extern const struct RefuelRelationships {
} RefuelRelationships;

extern const struct RefuelFetchedProperties {
} RefuelFetchedProperties;






@interface RefuelID : NSManagedObjectID {}
@end

@interface _Refuel : CostItem {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (RefuelID*)objectID;





@property (nonatomic, strong) NSString* brand;



//- (BOOL)validateBrand:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* isFull;



@property BOOL isFullValue;
- (BOOL)isFullValue;
- (void)setIsFullValue:(BOOL)value_;

//- (BOOL)validateIsFull:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDecimalNumber* liters;



//- (BOOL)validateLiters:(id*)value_ error:(NSError**)error_;






@end

@interface _Refuel (CoreDataGeneratedAccessors)

@end

@interface _Refuel (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveBrand;
- (void)setPrimitiveBrand:(NSString*)value;




- (NSNumber*)primitiveIsFull;
- (void)setPrimitiveIsFull:(NSNumber*)value;

- (BOOL)primitiveIsFullValue;
- (void)setPrimitiveIsFullValue:(BOOL)value_;




- (NSDecimalNumber*)primitiveLiters;
- (void)setPrimitiveLiters:(NSDecimalNumber*)value;




@end
