// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Service.h instead.

#import <CoreData/CoreData.h>
#import "CostItem.h"

extern const struct ServiceAttributes {
} ServiceAttributes;

extern const struct ServiceRelationships {
	__unsafe_unretained NSString *parts;
} ServiceRelationships;

extern const struct ServiceFetchedProperties {
} ServiceFetchedProperties;

@class PartItem;


@interface ServiceID : NSManagedObjectID {}
@end

@interface _Service : CostItem {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ServiceID*)objectID;





@property (nonatomic, strong) NSSet *parts;

- (NSMutableSet*)partsSet;





@end

@interface _Service (CoreDataGeneratedAccessors)

- (void)addParts:(NSSet*)value_;
- (void)removeParts:(NSSet*)value_;
- (void)addPartsObject:(PartItem*)value_;
- (void)removePartsObject:(PartItem*)value_;

@end

@interface _Service (CoreDataGeneratedPrimitiveAccessors)



- (NSMutableSet*)primitiveParts;
- (void)setPrimitiveParts:(NSMutableSet*)value;


@end
