// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to OtherItem.h instead.

#import <CoreData/CoreData.h>
#import "Item.h"

extern const struct OtherItemAttributes {
} OtherItemAttributes;

extern const struct OtherItemRelationships {
	__unsafe_unretained NSString *other;
} OtherItemRelationships;

extern const struct OtherItemFetchedProperties {
} OtherItemFetchedProperties;

@class Other;


@interface OtherItemID : NSManagedObjectID {}
@end

@interface _OtherItem : Item {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (OtherItemID*)objectID;





@property (nonatomic, strong) Other *other;

//- (BOOL)validateOther:(id*)value_ error:(NSError**)error_;





@end

@interface _OtherItem (CoreDataGeneratedAccessors)

@end

@interface _OtherItem (CoreDataGeneratedPrimitiveAccessors)



- (Other*)primitiveOther;
- (void)setPrimitiveOther:(Other*)value;


@end
