// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Wash.m instead.

#import "_Wash.h"

const struct WashAttributes WashAttributes = {
};

const struct WashRelationships WashRelationships = {
};

const struct WashFetchedProperties WashFetchedProperties = {
};

@implementation WashID
@end

@implementation _Wash

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Wash" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Wash";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Wash" inManagedObjectContext:moc_];
}

- (WashID*)objectID {
	return (WashID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}









@end
