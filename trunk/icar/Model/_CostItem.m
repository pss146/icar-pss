// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CostItem.m instead.

#import "_CostItem.h"

const struct CostItemAttributes CostItemAttributes = {
	.date = @"date",
	.location = @"location",
	.mileage = @"mileage",
	.price = @"price",
};

const struct CostItemRelationships CostItemRelationships = {
	.note = @"note",
};

const struct CostItemFetchedProperties CostItemFetchedProperties = {
};

@implementation CostItemID
@end

@implementation _CostItem

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"CostItem" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"CostItem";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"CostItem" inManagedObjectContext:moc_];
}

- (CostItemID*)objectID {
	return (CostItemID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic date;






@dynamic location;






@dynamic mileage;






@dynamic price;






@dynamic note;

	






@end
