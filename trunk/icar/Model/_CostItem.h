// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CostItem.h instead.

#import <CoreData/CoreData.h>


extern const struct CostItemAttributes {
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *location;
	__unsafe_unretained NSString *mileage;
	__unsafe_unretained NSString *price;
} CostItemAttributes;

extern const struct CostItemRelationships {
	__unsafe_unretained NSString *note;
} CostItemRelationships;

extern const struct CostItemFetchedProperties {
} CostItemFetchedProperties;

@class Note;






@interface CostItemID : NSManagedObjectID {}
@end

@interface _CostItem : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (CostItemID*)objectID;





@property (nonatomic, strong) NSDecimalNumber* date;



//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* location;



//- (BOOL)validateLocation:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDecimalNumber* mileage;



//- (BOOL)validateMileage:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDecimalNumber* price;



//- (BOOL)validatePrice:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Note *note;

//- (BOOL)validateNote:(id*)value_ error:(NSError**)error_;





@end

@interface _CostItem (CoreDataGeneratedAccessors)

@end

@interface _CostItem (CoreDataGeneratedPrimitiveAccessors)


- (NSDecimalNumber*)primitiveDate;
- (void)setPrimitiveDate:(NSDecimalNumber*)value;




- (NSString*)primitiveLocation;
- (void)setPrimitiveLocation:(NSString*)value;




- (NSDecimalNumber*)primitiveMileage;
- (void)setPrimitiveMileage:(NSDecimalNumber*)value;




- (NSDecimalNumber*)primitivePrice;
- (void)setPrimitivePrice:(NSDecimalNumber*)value;





- (Note*)primitiveNote;
- (void)setPrimitiveNote:(Note*)value;


@end
