// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PartItem.m instead.

#import "_PartItem.h"

const struct PartItemAttributes PartItemAttributes = {
};

const struct PartItemRelationships PartItemRelationships = {
	.services = @"services",
};

const struct PartItemFetchedProperties PartItemFetchedProperties = {
};

@implementation PartItemID
@end

@implementation _PartItem

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"PartItem" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"PartItem";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"PartItem" inManagedObjectContext:moc_];
}

- (PartItemID*)objectID {
	return (PartItemID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic services;

	
- (NSMutableSet*)servicesSet {
	[self willAccessValueForKey:@"services"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"services"];
  
	[self didAccessValueForKey:@"services"];
	return result;
}
	






@end
