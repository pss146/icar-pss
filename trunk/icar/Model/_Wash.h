// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Wash.h instead.

#import <CoreData/CoreData.h>
#import "CostItem.h"

extern const struct WashAttributes {
} WashAttributes;

extern const struct WashRelationships {
} WashRelationships;

extern const struct WashFetchedProperties {
} WashFetchedProperties;



@interface WashID : NSManagedObjectID {}
@end

@interface _Wash : CostItem {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (WashID*)objectID;






@end

@interface _Wash (CoreDataGeneratedAccessors)

@end

@interface _Wash (CoreDataGeneratedPrimitiveAccessors)


@end
