// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Item.h instead.

#import <CoreData/CoreData.h>


extern const struct ItemAttributes {
	__unsafe_unretained NSString *name;
} ItemAttributes;

extern const struct ItemRelationships {
	__unsafe_unretained NSString *category;
	__unsafe_unretained NSString *childs;
	__unsafe_unretained NSString *parent;
} ItemRelationships;

extern const struct ItemFetchedProperties {
} ItemFetchedProperties;

@class CostCategory;
@class Item;
@class Item;



@interface ItemID : NSManagedObjectID {}
@end

@interface _Item : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ItemID*)objectID;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) CostCategory *category;

//- (BOOL)validateCategory:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *childs;

- (NSMutableSet*)childsSet;




@property (nonatomic, strong) Item *parent;

//- (BOOL)validateParent:(id*)value_ error:(NSError**)error_;





@end

@interface _Item (CoreDataGeneratedAccessors)

- (void)addChilds:(NSSet*)value_;
- (void)removeChilds:(NSSet*)value_;
- (void)addChildsObject:(Item*)value_;
- (void)removeChildsObject:(Item*)value_;

@end

@interface _Item (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;





- (CostCategory*)primitiveCategory;
- (void)setPrimitiveCategory:(CostCategory*)value;



- (NSMutableSet*)primitiveChilds;
- (void)setPrimitiveChilds:(NSMutableSet*)value;



- (Item*)primitiveParent;
- (void)setPrimitiveParent:(Item*)value;


@end
