// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PartItem.h instead.

#import <CoreData/CoreData.h>
#import "Item.h"

extern const struct PartItemAttributes {
} PartItemAttributes;

extern const struct PartItemRelationships {
	__unsafe_unretained NSString *services;
} PartItemRelationships;

extern const struct PartItemFetchedProperties {
} PartItemFetchedProperties;

@class Service;


@interface PartItemID : NSManagedObjectID {}
@end

@interface _PartItem : Item {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (PartItemID*)objectID;





@property (nonatomic, strong) NSSet *services;

- (NSMutableSet*)servicesSet;





@end

@interface _PartItem (CoreDataGeneratedAccessors)

- (void)addServices:(NSSet*)value_;
- (void)removeServices:(NSSet*)value_;
- (void)addServicesObject:(Service*)value_;
- (void)removeServicesObject:(Service*)value_;

@end

@interface _PartItem (CoreDataGeneratedPrimitiveAccessors)



- (NSMutableSet*)primitiveServices;
- (void)setPrimitiveServices:(NSMutableSet*)value;


@end
