// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Parking.m instead.

#import "_Parking.h"

const struct ParkingAttributes ParkingAttributes = {
	.period = @"period",
};

const struct ParkingRelationships ParkingRelationships = {
};

const struct ParkingFetchedProperties ParkingFetchedProperties = {
};

@implementation ParkingID
@end

@implementation _Parking

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Parking" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Parking";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Parking" inManagedObjectContext:moc_];
}

- (ParkingID*)objectID {
	return (ParkingID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic period;











@end
