// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to OtherItem.m instead.

#import "_OtherItem.h"

const struct OtherItemAttributes OtherItemAttributes = {
};

const struct OtherItemRelationships OtherItemRelationships = {
	.other = @"other",
};

const struct OtherItemFetchedProperties OtherItemFetchedProperties = {
};

@implementation OtherItemID
@end

@implementation _OtherItem

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"OtherItem" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"OtherItem";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"OtherItem" inManagedObjectContext:moc_];
}

- (OtherItemID*)objectID {
	return (OtherItemID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic other;

	






@end
