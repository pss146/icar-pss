// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Refuel.m instead.

#import "_Refuel.h"

const struct RefuelAttributes RefuelAttributes = {
	.brand = @"brand",
	.isFull = @"isFull",
	.liters = @"liters",
};

const struct RefuelRelationships RefuelRelationships = {
};

const struct RefuelFetchedProperties RefuelFetchedProperties = {
};

@implementation RefuelID
@end

@implementation _Refuel

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Refuel" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Refuel";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Refuel" inManagedObjectContext:moc_];
}

- (RefuelID*)objectID {
	return (RefuelID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"isFullValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isFull"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic brand;






@dynamic isFull;



- (BOOL)isFullValue {
	NSNumber *result = [self isFull];
	return [result boolValue];
}

- (void)setIsFullValue:(BOOL)value_ {
	[self setIsFull:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsFullValue {
	NSNumber *result = [self primitiveIsFull];
	return [result boolValue];
}

- (void)setPrimitiveIsFullValue:(BOOL)value_ {
	[self setPrimitiveIsFull:[NSNumber numberWithBool:value_]];
}





@dynamic liters;











@end
