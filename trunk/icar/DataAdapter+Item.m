//
// Created by Stanislav Perepelitsyn on 3/21/14.
// Copyright (c) 2014 Stanislav Perepelitsyn. All rights reserved.
//

#import "DataAdapter+Item.h"


@implementation DataAdapter (Item)

/*
    Find all items with specified name
 */
- (NSArray *)findItemsByName:(NSString *)name {
    NSLog(@">>> findItemsByName: %@", name);
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"name LIKE %@", name];
    NSFetchedResultsController * result = [Item fetchAllGroupedBy:nil withPredicate:predicate sortedBy:@"name" ascending:YES];

    NSLog(@"Found %d items", result.fetchedObjects.count);
    return result.fetchedObjects;
}

/*
    Find all root items in specified category.
    It is the fist point to start from when view items hierarchy
 */
- (NSFetchedResultsController *)findRootItemsInCategory:(CostCategory *)category delegate:(id <NSFetchedResultsControllerDelegate>)delegate {
    NSLog(@">>> findRootItemsInCategory:delegate: %@", category.name);
    if (category == nil) return nil;
    NSManagedObjectID * categoryObjectID = [category objectID];

    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"parent == nil && category == %@", categoryObjectID];
    NSFetchedResultsController * result = [Item fetchAllGroupedBy:nil withPredicate:predicate sortedBy:@"name" ascending:YES delegate:delegate];

    NSLog(@"Found %d items", result.fetchedObjects.count);
    return result;
}

/*
    Find all items in hierarchy matching parent item
 */
- (NSFetchedResultsController *)findItemsByParent:(Item *)item delegate:(id <NSFetchedResultsControllerDelegate>)delegate {
    NSLog(@">>> findItemsByParent:delegate: %@", item.name);
    if (item == nil) return nil;
    NSManagedObjectID * parentObjectID = [item objectID];

    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"(parent != nil && parent = %@)", parentObjectID];
    NSFetchedResultsController * result = [Item fetchAllGroupedBy:nil withPredicate:predicate sortedBy:@"name" ascending:YES delegate:delegate];

    NSLog(@"Found %d items by predicate: %@", result.fetchedObjects.count, predicate);
    return result;
}


@end