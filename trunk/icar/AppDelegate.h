//
//  AppDelegate.h
//  icar
//
//  Created by Stanislav Perepelitsyn on 2/16/14.
//  Copyright (c) 2014 Stanislav Perepelitsyn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
