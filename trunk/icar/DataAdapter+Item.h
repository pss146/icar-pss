//
// Created by Stanislav Perepelitsyn on 3/21/14.
// Copyright (c) 2014 Stanislav Perepelitsyn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataAdapter.h"

@interface DataAdapter (Item)

-(NSArray *)findItemsByName:(NSString *)name;

- (NSFetchedResultsController *)findRootItemsInCategory:(CostCategory *)category delegate:(id <NSFetchedResultsControllerDelegate>)delegate;

- (NSFetchedResultsController *)findItemsByParent:(Item *)parent delegate:(id <NSFetchedResultsControllerDelegate>)delegate;

@end