//
//  DataAdapter.h
//  icar
//
//  Created by Stanislav Perepelitsyn on 3/17/14.
//  Copyright (c) 2014 Stanislav Perepelitsyn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataModel.h"

@interface DataAdapter : NSObject

+ (DataAdapter*)sharedInstance; // Singletone
- (void)saveContext;

-(CostCategory *) serviceCategory;
-(CostCategory *) otherCategory;

@end
