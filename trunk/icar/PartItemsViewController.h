//
//  PartItemsViewController.h
//  icar
//
//  Created by Stanislav Perepelitsyn on 3/19/14.
//  Copyright (c) 2014 Stanislav Perepelitsyn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model/DataModel.h"

@interface PartItemsViewController : UITableViewController <NSFetchedResultsControllerDelegate>
@property (nonatomic, retain) CostCategory * costCategory;
@property (nonatomic, retain) Item * hierarchyItem;
@property (nonatomic, retain) NSFetchedResultsController * items;
@end
