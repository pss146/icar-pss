//
//  PartItemsViewController.m
//  icar
//
//  Created by Stanislav Perepelitsyn on 3/19/14.
//  Copyright (c) 2014 Stanislav Perepelitsyn. All rights reserved.
//

#import "PartItemsViewController.h"
#import "DataAdapter.h"
#import "DataAdapter+Item.h"

@interface PartItemsViewController ()
-(void) loadData;
@end

@implementation PartItemsViewController

@synthesize items = _items;
@synthesize hierarchyItem = _hierarchyItem;
@synthesize costCategory = _costCategory;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSFetchedResultsController *)items {
    if (_items != nil) return _items;

    if (self.costCategory != nil) {
        // Load data for root category
        NSLog(@"Load items for costCategory: %@", self.costCategory.name);
        self.items = [[DataAdapter sharedInstance] findRootItemsInCategory:self.costCategory delegate:self];
    } else if (self.hierarchyItem != nil) {
        // Load data for selected hierarchy level
        NSLog(@"Load items for hierarchy: %@", self.hierarchyItem.name);
        self.items = [[DataAdapter sharedInstance] findItemsByParent:self.hierarchyItem delegate:self];
    }

    return _items;
}

-(void)loadData {

    if (self.costCategory != nil) {
        self.navigationItem.title = self.costCategory.name;
    } else if (self.hierarchyItem != nil) {
        self.navigationItem.title = self.hierarchyItem.name;
    } else {
        // DEBUG
        // By default load items for serviceCategory
        CostCategory * serviceCategory = [[DataAdapter sharedInstance] serviceCategory];
        self.costCategory = serviceCategory;
        self.navigationItem.title = self.costCategory.name;
    }

    //[self.tableView reloadData];
    NSLog(@"Items count: %d", self.items.fetchedObjects.count);
    [self.items performFetch:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.items.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.fetchedObjects.count;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {

    Item *item = [self.items objectAtIndexPath:indexPath];
    NSLog(@"ConfigureCell atIndexPath: %@", indexPath);

    if ([[cell reuseIdentifier] isEqualToString:@"itemCell"]) {
        cell.textLabel.text = item.name;
        cell.detailTextLabel.text = nil;
    } else {
        cell.textLabel.text = item.name;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"(%d)", item.childs.count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    Item* item = [self.items objectAtIndexPath:indexPath];

    if ([item isKindOfClass:[PartItem class]]) {
        NSString *cellId = (item.childs.count == 0) ? @"itemCell" : @"groupCell";
        cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
        NSAssert(cell != nil, @"Cell == nil");

        [self configureCell:cell atIndexPath:indexPath];
    } else {
        NSLog(@"Item not a PartItem class, real = %@", [Item class]);
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    Item* selectedItem = [self.items objectAtIndexPath:indexPath];
    NSLog(@"Item selected: %@", selectedItem.name);

    if (selectedItem.childs.count > 0) {
        // Invoke segue to load next hierarchy level
        [self performSegueWithIdentifier:@"loopTable" sender:selectedItem];
    }
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        NSLog(@"count before delete: %d", self.items.fetchedObjects.count);
        Item* item = [self.items objectAtIndexPath:indexPath];
        NSLog(@"Item about to be deleted: %@", item.name);
        [item MR_deleteEntity];
        //[[DataAdapter sharedInstance] saveContext];
        [[item managedObjectContext] saveToPersistentStoreAndWait];

        //[self setItems: nil];
        //[self.items performFetch:nil];
        NSLog(@"count after delete: %d", self.items.fetchedObjects.count);

        //[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //[super prepareForSegue:segue sender:sender];

    if ([segue.identifier isEqualToString:@"loopTable"]) {
        // Go to next hierarchy level -> Set data for next screen

        PartItemsViewController *destViewControler = segue.destinationViewController;
        if ([destViewControler isKindOfClass:[UINavigationController class]]) {
            destViewControler = [segue.destinationViewController topViewController];
        }
        destViewControler.hierarchyItem = (Item *)sender;
    }
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.tableView;
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
}

@end
