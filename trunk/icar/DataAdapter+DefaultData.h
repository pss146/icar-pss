//
// Created by Stanislav Perepelitsyn on 3/17/14.
// Copyright (c) 2014 Stanislav Perepelitsyn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataAdapter.h"

static const NSString* SERVICE_CATEGORY_NAME = @"Service";
static const NSString* OTHER_CATEGORY_NAME = @"Other";

@interface DataAdapter (DefaultData)

-(void)createDefaultDataInDatabase;
@end